# Base
	-- navbar with:
		-- home
		-- login
		-- registration
		-- logout
		-- mode switch
		-- search
		-- create new note


# Main page:
	-- show all cards
		##  for each card:
				-- show title
				-- show text
				-- show date
				-- show creator
				-- show links to tags
				-- show links to refs
				## if user is creator:
					-- edit button
					-- delete button
	-- implement random carousel


# Note page:
	-- show card content
	-- edit button
	-- delete button
	-- links to tags
	-- links to refs


# Edit page:
	-- shows only if user logged in is creator


# Delete page:
	-- confirm deletion


# New note page:
	-- create new tags if not already present in db
	-- create new refs if not already present in db


# Search page:
	-- search byb tags
	-- search by refs
	-- search by title
	-- search by content


# Tags page:
	-- /tags show all tags and links to notes
	-- /tags/{tagname}:
		-- show {tagname} and links to all notes


# Refs page:
	-- /refs show all refs and links to notes
	-- /refs/{refname}:
		-- show {refname} and links to all notes
