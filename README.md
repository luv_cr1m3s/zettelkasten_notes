# Zettelkasten_notes

## Django project implementation of [Zettelkasten_notes](https://www.taskade.com/blog/zettelkasten-method-for-distributed-teams/)

## Deployed at [pythonanywhere](https://cr1m3s.pythonanywhere.com/)

![Note](https://zettelkasten.de/posts/baseline-zettelkasten-software-reviews/202010031149_zettel-anatomy.png)
