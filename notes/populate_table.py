import os
from datetime import date
from random import randint

os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      'notes.settings')

import django
django.setup()
from zettelkasten.models import ZettelkastenNote, Tag, Reference


def add_tag(name):
    c = Tag.objects.get_or_create(name=name)[0]

    c.save()
    return c

def add_reference(name, url):
    r = Reference.objects.get_or_create(title=name)[0]
    r.url = url

    r.save()
    return r

def add_note(tag, ref, title, text, date):

    n = ZettelkastenNote.objects.get_or_create(title = title)[0]
    n.text = text
    n.date = date
    n.tags.add(tag)
    n.refs.add(ref)

    n.save()
    return n

def populate():


    tags = ["history", "art", "math", "chemistry", "compsci"]
    
    ref = add_reference("wikibook", "https://www.w3schools.com/python/python_datetime.asp")

    text = """Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
              sed do eiusmod tempor incididunt ut labore et dolore magna
              aliqua. Ut enim ad minim veniam, quis nostrud exercitation
              ullamco laboris nisi ut aliquip ex ea commodo consequat. 
              Duis aute irure dolor in reprehenderit in voluptate velit
              esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
              occaecat cupidatat non proident, sunt in culpa qui officia
              deserunt mollit anim id est laborum."""
   
    title = [f"{i} title"  for i in tags]

    for i in range(0, len(tags)):
        tag = add_tag(tags[i])
        add_note(tag, ref, title[i], text, date=date.today())

if __name__ == '__main__':
    populate()
