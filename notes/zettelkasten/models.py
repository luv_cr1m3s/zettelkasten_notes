from django.db import models
from datetime import date
from django.contrib.auth.models import User
from taggit.managers import TaggableManager
# Create your models here.

class ZettelkastenNote(models.Model):
    title = models.CharField(max_length=128)
    text = models.TextField()
    creator = models.ForeignKey(User, on_delete=models.CASCADE, null=True, default=None)
    date = models.DateField(default = date.today)
    refs = models.CharField(max_length=128, null=True, default = None, blank=True)
    
    tags = TaggableManager()

    def __str__(self):
        return self.title
