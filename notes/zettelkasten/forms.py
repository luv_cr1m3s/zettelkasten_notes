from django import forms
from django.contrib.auth.models import User 
from django.contrib.auth.forms import UserCreationForm
from .models import ZettelkastenNote

class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        fields = UserCreationForm.Meta.fields + ("email",)

class NoteForm(forms.ModelForm):

    class Meta:
        model = ZettelkastenNote
        fields = ['title', 'text', 'tags', 'creator', 'date', 'refs']

    def __init__(self, *args, **kwargs):
        super(NoteForm, self).__init__(*args, **kwargs)
        self.fields['creator'].widget = forms.HiddenInput()
        self.fields['date'].widget = forms.HiddenInput()
