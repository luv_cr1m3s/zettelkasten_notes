from django.shortcuts import HttpResponse, render, redirect, get_object_or_404
from .models import ZettelkastenNote
from .forms import CustomUserCreationForm, NoteForm
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from datetime import datetime
from taggit.models import Tag
from django.db.models import Q
from random import shuffle
# Create your views here.

def dashboard(request):
    return render(request, "dashboard.html")

def register(request):

    if request.method == "GET":
        return render(
            request, "register.html",
            {"form": CustomUserCreationForm}
        )
    elif request.method == "POST":
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect(reverse("dashboard"))
        else:
            return HttpResponse(f"Something is wrong with: {form.errors}")


def note_list(request):
    search_post = request.GET.get('search')
    if search_post:
        notes = ZettelkastenNote.objects.filter(Q(title__icontains=search_post) & Q(text__icontains=search_post))
        notes |= ZettelkastenNote.objects.filter(tags__name=search_post.lower())
        notes = list(notes)
        if len(notes) == 0:
            return HttpResponse("Nothing found")
        return render(request, 'search_results.html', {'notes': notes})

    notes = ZettelkastenNote.objects.all()
    notes = list(notes)
    shuffle(notes)
    return render(request, 'note_list.html', {'notes': notes})


def note_details(request, note_id):
    note = get_object_or_404(ZettelkastenNote, pk=note_id)

    return render(request, 'note_details.html', {'note': note})


@login_required(login_url='/accounts/login/')
def note_edit(request, note_id):
    note = get_object_or_404(ZettelkastenNote, pk=note_id)
    
    if request.method == 'POST':
        form = NoteForm(request.POST, instance=note)
        if form.is_valid():
            print("valid")
            form.save()
            return redirect('note_list')
        else:
            return HttpResponse(f"something wrong: {form.errors}")
    
    form = NoteForm(instance=note)
    username = request.user

    return render(request, 'note_edit.html', {'form': form, 'note': note, 'username': username})


@login_required(login_url='/accounts/login/')
def note_create(request):
    date = datetime.now()
    user = request.user
    
    form = NoteForm(request.POST, initial={'date': date, 'creator': user})
    
    form.fields['creator'].disabled = True
    form.fields['date'].disabled = True

    if request.method == 'POST':
        if form.is_valid():
            form.save()
        else:
            return HttpResponse(f"something wrong: {form.errors}")
        return redirect('note_list')
    return render(request, 'note_create.html', {'form': form, 'username': user})


@login_required(login_url='/accounts/login/')
def note_delete(request, note_id):
    note = get_object_or_404(ZettelkastenNote, pk=note_id)

    if request.method == 'POST':
        if 'confirm' in request.POST:
            note.delete()
        return redirect('note_list')
    return render(request, 'delete_note.html', {'note': note})


def tags_list(request):
    tags = Tag.objects.all()
    result = {}
    for tag in tags:
        notes = ZettelkastenNote.objects.filter(tags__name=tag.name)
        if len(notes) != 0:
            result[tag.name] = notes

    return render(request, 'tags_list.html', {'result': result})

def tag_details(request, tag_name):
    tag = get_object_or_404(Tag, name=tag_name)
    notes = ZettelkastenNote.objects.filter(tags__name=tag_name)
    print(notes)
    print(tag)
    return render(request, 'tag_details.html', {'tag': tag, 'notes': notes})